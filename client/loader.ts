class Loader{

    calcTimer;

    start(){
        const P = ["\\", "|", "/", "-"];
        let x = 0;
        return this.calcTimer = setInterval(function() {
          process.stdout.write("\r wait, calculating " + P[x++]);
          x &= 3;
        }, 200);
    };

    stop(){
        clearInterval(this.calcTimer);
        process.stdout.write("\r");
    }
}

export default Loader;