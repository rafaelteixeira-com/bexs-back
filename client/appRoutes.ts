import fs from 'fs';
import axios from 'axios';
import colors from 'colors';
import Loader from './loader';
import Prompters from './prompters';

axios.defaults.withCredentials = true;

class AppRoutes extends Prompters{

    BASE_URL = "http://localhost:3333/route";
    axiosInstance = axios.create({ baseURL: this.BASE_URL});
    axiosInstanceCookie = '';

    constructor(){
        super();
        if(process.env.NODE_ENV == 'test') return;
        this.axiosInstance.defaults.headers.connection = 'keep-alive'; 
        this.loader = new Loader();
        this.axiosInstance.defaults.headers.contenttype = 'application/json; charset=utf-8';  
        this.routesPath = process.argv[2];
        if(this.routesPath){
            this.initRouteApp();
        }
        else {
            console.log(colors.red(`Please select a csv file, containing a list of routes in the format "GRU,BRC,10" like "routes.csv"`));
            console.log(colors.yellow(`Use the command: npm run routesCheck`), colors.yellow.bold(`path/file.csv`) );
            this.exit();
        }

    }

    async initRouteApp(){
        await this.getRoutesContent().then(
            async () => {
                await this.convertRoutesContentToContext().then(
                    () => {
                        this.initPromptNavigation();
                    }
                );
            }
        ).catch( (e) =>{
            this.exit();
        });

    }

    async getRoutesContent(){
        return await new Promise( async(resolve, reject) => {
            await fs.readFile( this.routesPath, 'utf8', async(err, data) => {
                if(err) reject(console.log(colors.yellow(`An error occurred while trying to open the file: `) + err));
                resolve(this.routesContent = data);
            });
        });   
    }

    async setRoutesContent(listRoutes, resolved){
        return await new Promise( (resolve, reject) => {
            fs.writeFile( this.routesPath, listRoutes, async(err) => {
                if(err) reject(this.exit().error(colors.yellow(`An error occurred while trying to write the file: `) + err));
                resolve( resolved );
            }); 
        });   
    }

    async convertRoutesContentToContext(){
        let splitedRowsContent:any = this.routesContent.split('\n');
        splitedRowsContent.map( item => {
            item = item.split(',');
            let value = item.splice(-1,1);
            let route = item;
            this.routesList.push({
                route,
                value: value[0]
            });
        });
    
        return await this.sendListRoutes(this.routesList).then((res) => {
            console.log(colors.green(`${this.routesPath} file uploaded successfully`));
            this.axiosInstanceCookie = res['headers']['set-cookie'][0].split(';')[0];
        });
    }


    async getCalculedRoute(route){
        //console.log('getCalculedRoute', route);
        return await this.axiosInstance.get('/calc', {
            withCredentials: true,
            params: {
                route
            },
            headers:{
                'cookie': this.axiosInstanceCookie
            }
        }).catch((error) => {
            if( error.hasOwnProperty('response') && error.response.hasOwnProperty('data') && error.response.data.hasOwnProperty('msg') ) console.error( colors.red(error.response.data.msg) );
            else console.error(colors.red('Error on service calc:'), error.response.status, error.response.statusText );
        })    
    }


    async getListRoutes(){
        return await this.axiosInstance.get('http://localhost:3333/route/list', 
        {
            withCredentials: true,
            headers:{
                'cookie': this.axiosInstanceCookie
            }
        }).catch((error) => {
            console.error(error);
        })    
    }

    async sendListRoutes(newRoute){
        return await this.axiosInstance.post('/list',
            {
                list: newRoute
            },
            {
                withCredentials: true,
                headers:{
                    'cookie': this.axiosInstanceCookie
                }
            }
        ).catch((error) => {
            console.error(error.code, error.errno, error.response)
        });
    
    }


    exit(){
        return {
            error: function(msg){
                if(msg) console.log( colors.red(msg) );
                process.exit();
            },
            warning: function(msg){
                if(msg) console.log( colors.yellow(msg) );
                process.exit();
            }
        }
    }
}

export default new AppRoutes();







