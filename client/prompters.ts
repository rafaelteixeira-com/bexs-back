import prompts from 'prompts';
import colors from 'colors';

class Prompters{

    routesPath;
    routesContent:String;
    routesContext:Array<any> = [];
    routesList:Array<any> = [];
    loader;


    async initPromptNavigation(){
        if(process.env.NODE_ENV == 'test') return;
        const response = await prompts([
            {
                type: 'select',
                name: 'optionNav',
                initial: 0,
                instructions: false,
                message: 'How do you want to continue?',
                max: 1,
                hint: 'Use 🡩 or 🡫 and ⥐ Space or ⤶ Return (Enter) to select',
                choices: [
                    { title: 'Consult the cheapest price of a route', value: 'consult' },
                    { title: 'Insert a new route', value: 'new' },
                    { title: 'Save the updated data to the file', value: 'save' },
                    { title: 'Exit', value: 'exit' }
                ],
            }
        ]);
        if(response.hasOwnProperty('optionNav')){
            switch(response.optionNav){
                case 'new':
                    this.promptInsert();
                    break;
                case 'consult':
                    this.promptConsult();
                    break;
                case 'save':
                    this.promptSave();
                    break;
                case 'exit':
                    console.log(colors.cyan(`Thanks for the challenge, Bexs!`));
                    this.exit();
                    break;
                default:
                    console.log(colors.yellow(`Please, select a option with Space on your keyboard (-)`));
                    this.initPromptNavigation();
            }
        }
    }

    async promptConsult(){
       
        console.log(colors.gray('Write a route FROM-TO like: GRU-FOR. Write exit or ^c to back the menu'));
        const response = await prompts([
            {
            type: 'text',
            name: 'route',
            message: 'please enter the route'
            }
        ]);
        
        const patternRouteValidation = /^[a-zA-Z]{3,3}-[a-zA-Z]{3,3}$/;

        if(response.hasOwnProperty('route') && response.route !== 'exit'){
            if(!response.route.match(patternRouteValidation)){
                console.log(colors.red('The route is not in the standard XXX-XXX format.'));
                console.log(colors.yellow('Please, try again.'));
                this.promptConsult();
            }else{
                this.loader.start();
                const partsRoute = response.route.split('-');
                const result = this.getCalculedRoute(partsRoute).then( result =>{
                    this.loader.stop();
                    if(result) console.log(colors.green('best route: ' + colors.bold(result['data'].route.join(' - ') + ' > $'+result['data'].value)) );
                    this.promptConsult();
                });
            }
        }
        else if(!response.hasOwnProperty('route')) this.initPromptNavigation();
        else this.initPromptNavigation();
    }

    async promptSave(){
       
        console.log(colors.blue(`Saving List Routes into ${ this.routesPath } `));
        this.loader.start();
        await this.getListRoutes().then( async(result) => {
            if(result && result.hasOwnProperty('data') && result.data.hasOwnProperty('list')){
                this.loader.stop();
                let listPrepared = result.data.list.map( row => {
                    return row.route.join(',') + ',' + row.value;
                }).join('\n');
                await this.setRoutesContent(listPrepared, () => { });
                const response = await prompts([
                    {
                        type: 'toggle',
                        name: 'value',
                        message: colors.blue(`list Saved on ${ this.routesPath }. Do you want to continue? `),
                        initial: true,
                        active: 'yes',
                        inactive: 'no'
                    }
                ]);
                if(response.hasOwnProperty('value') && (response.value == 'no' || response.value === false)){
                    console.log(colors.cyan(`Thanks for the challenge, Bexs!`));
                    this.exit();
                } else{
                    this.initPromptNavigation(); 
                }
            } else{
                this.initPromptNavigation();
            }
        });
    }

    async promptInsert(){
       
        console.log(colors.gray('Write a route FROM-CONECTION?-TO,VALUE like: GRU-FOR,2 or GRU-GIG-FOR,10 or GRU-SDU-IOS-FOR,5. Write exit or ^c to back the menu'));
        const response = await prompts([
            {
            type: 'text',
            name: 'route',
            message: 'please insert the new route'
            }
        ]);
        
        if(response.hasOwnProperty('route') && response.route !== 'exit'){

            const patternRouteValidation = /^[a-zA-Z]{3,3}(\-[a-zA-Z]{3,3}){1,15}?,[0-9]{1,10}$/;

            if(response.route.match(patternRouteValidation)){
                const partsRoute = response.route.split(',');
                let partsRouteAirports = partsRoute[0].split('-');
                partsRouteAirports = partsRouteAirports.map( route => {
                    return route.toUpperCase();
                })
                const partsRouteValue = partsRoute[1];
                const newRoute = [{
                        route: partsRouteAirports,
                        value: partsRouteValue
                }];
                this.loader.start();
                await this.sendListRoutes(newRoute).then( (res) => {
                    //console.log(colors.magenta(`result`, res['data']['result']));
                    console.log(colors.green(`route ${ colors.bold(partsRouteAirports.join(' - ') + ' > $'+partsRouteValue) } inserted successfully`));
                });
                this.loader.stop();
                this.promptInsert();
            }else{
                console.log(colors.red('The route is not in the standard XXX-XXX,00 format.'));
                console.log(colors.yellow('Please, try again.'));
                this.promptInsert();
            }
            
        }
        else if(!response.hasOwnProperty('route')) this.initPromptNavigation();
        else this.initPromptNavigation();
    }


}

export default Prompters;