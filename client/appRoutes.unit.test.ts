import AppRoutes from './appRoutes';
import request from 'supertest';
process.argv.push('./client/routes.csv');

describe("AppRoutes", () => {
 
 
  it("should back service running on another terminal (npm run appRoutesBack)", async () => {

    const response = await request('http://localhost:3333/route')
      .get("/list")
      .send();
    
      expect(response.status).toBe(200);
  });

  it("should read file csv", async () => {

    AppRoutes.routesPath = './client/routes.csv';
    await AppRoutes.getRoutesContent();
  
    expect(AppRoutes.routesContent).toContain(',');
  });
 
  it("should get calc of route", async () => {

    AppRoutes.routesPath = './client/routes.csv';
    await AppRoutes.initRouteApp();
    const response = await AppRoutes.getCalculedRoute(['IOS', 'CGH']);
    
    expect(response['data']).toMatchObject({ route: expect.any(Array), value: expect.any(String)});
  });
});