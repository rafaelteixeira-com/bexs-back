class RoutesManagerController{

  routesList = [];

  constructor() {

  }

  async calc(req, res){
    const listRoutes = req.session.listRoutes;
    // console.log('listRoutes', req.session.listRoutes);
    if( req.session.listRoutes) console.log('calc listRoutes length', req.session.listRoutes.length);

    if(!req.session.listRoutes){
      return res.status(500).json({ msg:'A sessão expirou. ' });
    }
    const { route } = req.query;

    if(route.length === 2){
      route[0] = route[0].toUpperCase();
      route[1] = route[1].toUpperCase();
      let cheapestControl = 0;
      const filteredRoutes = listRoutes.reduce( ( prevRoute, routeRow ) => {
        if(!prevRoute) return routeRow;
        //console.log('filteredRoutes', filteredRoutes);
        const foundedRow = ( routeRow.route[0].toUpperCase() == route[0] && routeRow.route[routeRow.route.length-1].toUpperCase() == route[1]);
        return (
          (prevRoute.route[0].toUpperCase() !== route[0] || prevRoute.route[prevRoute.route.length-1].toUpperCase() !== route[1]) ||
          foundedRow && ( parseFloat(routeRow.value) <= parseFloat(prevRoute.value) )
        ) ? routeRow : prevRoute;
      });
      if(!filteredRoutes || (filteredRoutes && filteredRoutes.route[0].toUpperCase() !== route[0] || filteredRoutes.route[filteredRoutes.route.length-1].toUpperCase() !== route[1] ) ) return res.status(404).json({ msgError:'Sorry, Route not found' });
      console.log('Founded Route: ', filteredRoutes);
      return res.status(200).json(filteredRoutes);
    }else{
      return res.status(500).json({ msg:'Error on format Route' });
    }
  }

  async setNewRoute(req, res){
    console.log('setNewRoute');
    const sessionList = req.session;
    return res.status(201).json({ result: '' });
  }

  async getListRoutes(req, res){
    console.log('getListRoutes');
    req.session.views = (req.session.views || 0) + 1
    return res.status(200).json({ list: req.session.listRoutes });
  }
ios
  async setListRoutes(req, res){
    let { list } = req.body;
    //console.log('listRoutes' , req.headers, req.session.listRoutes, list);
    if(!req.session.listRoutes) req.session.listRoutes = list;
    else req.session.listRoutes.push(list[0]);
    req.session.views = (req.session.views || 0) + 1
    console.log('listRoutes' , list, req.session.listRoutes);
    return res.status(201).json({ result: req.session.listRoutes, views:req.session.views  });
  }

}

export default new RoutesManagerController();