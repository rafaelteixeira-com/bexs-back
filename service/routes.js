import { Router } from 'express';
import RoutesManagerController from './controllers/RoutesManagerController';

const routes = new Router();

routes.get('/route/calc', RoutesManagerController.calc);
routes.post('/route/list', RoutesManagerController.setListRoutes);
routes.get('/route/list', RoutesManagerController.getListRoutes);

export default routes;