
# Aplicação do desafio de back-end para a bexs #

# Rota de Viagem #

#### Antes de iniciar, execute:

npm install ou yarn install

#### Em seguida, rode em terminais separados:

**App Client:**
yarn run appRoutesBack

**App Back-end:**
yarn run appRoutesClient **<arquivo.csv>**
~~yarn run appRoutesClient client/routes.csv~~

**Siga as instruções de appRoutesClient para realizar as operações**

#### Para rodar os testes unitários
yarn test ou npm test


## Estrutura da aplicação

A aplicação foi dividida em duas partes: **Client** *(executada no console)* e a **service** *(serviço back-end rodando os serviços rest)*

Na parte **Client**, foi usado uma biblioteca prompts para agilizar/potencializar os inputs do usuário e outputs da aplicação, além do axios para gerenciar as requisições rest.

Ao acessar a aplicação, é apresentada um menu de navegação inicial com 4 opções: *Consulta, Inserir Nova Rota, Salvar (os dados inseridos durante a exe. da app) e Sair*

Na parte **Service**, foi aplicada a biblioteca express *(usada para gerenciar as requisições rest)* e express-session, usada para gerenciar os dados do arquivo csv e retornar a lista atualizada para o *Client*


## Design Patterns

A aplicação foi desenvolvida usando typescript orientada a objetos. Seguindo estrutura de classes e de métodos usando principalmente o padrão builder do design pattern GoF.

Foi aplicada a dependecia sucrase para rodar typescript, nodemon para realizar o hot-deploy em desenvolvimento e o Jest para aplicar alguns testes automatizados.

## API REST

**Foram criados 3 end-points:**

POST /route/list *(Para registrar rotas na aplicação)*
GET /route/calc *(Para calcular e retornar a melhor rota)*
GET /route/list *(Para retornar as rotas atualizadas na aplicação)*